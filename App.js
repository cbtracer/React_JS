import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import './App.css';
import FormulaOne from './components/FormulaOne.js';
import Jumbo from './components/Jumbo.js';
import Weather from './components/Weather.js';

class App extends Component {

constructor(props){
  super(props);
  this.state = {
    data: [],
    limit: 840,
    offset: 0,
    pages: 0,
    weather: [],
    error: "",
    place: [],
    timestamp: 0,
    active: false
  };
  this.loadData = this.loadData.bind(this);
  this.onRowClick = this.onRowClick.bind(this);
  this.findWeather = this.findWeather.bind(this);

}

loadData(url){
  fetch(url)
  .then(response => {
    return response.json();
  }).then(json => {
    let pageNumber = Math.round(json.MRData.total / this.state.limit);
    this.setState({
      data: json.MRData.DriverTable.Drivers,
      pages: pageNumber
    });
    // console.log(this.state);
  }).catch(err => {
    console.log(err);
  })
}



onRowClick(row){
  window.open(`${row.url}`, '_blank');
}

findWeather(e){
  e.preventDefault();
let city = document.getElementById('weather').value.toString();
city = city.charAt(0).toUpperCase() + city.slice(1);
city = city.replace(/ +/gi, '');

fetch(`${this.props.weatherUrl}${city}${this.props.apiKey}`)
.then(response => {
  return response.json();
}).then(json => {
  this.setState({
    weather: json.list,
    place: json.city,
    timestamp: (new Date(json.list[0].dt * 1000)).getDate(),
    active:true
  });
  if (json.message === 'Error'){
    this.setState({
      error: "Please enter a valid City"
    });
  }else {
    this.setState({
      error: ""
    });
  }
  // console.log(this.state.weather);
}).catch(err => {
  console.log(err);
})
}


componentWillMount(){
  this.loadData(`${this.props.baseUrl}?limit=${this.state.limit}&offset=${this.state.offset}`);
}




  render() {
    const options = {
    onRowClick: this.onRowClick,
    paginationSize: 8
  };

    return (
      <div className="App">
        <Jumbo />

        <FormulaOne />

        <div className="container">
          <BootstrapTable
            data={this.state.data}
            pagination={true}
            options={options}
            striped hover inverse>
            <TableHeaderColumn className="col-sm-3 text-center" columnClassName="col-sm-3" dataField='givenName' filter={{type:'TextFilter', delay:0}}>First Name</TableHeaderColumn>
            <TableHeaderColumn className="col-sm-3 text-center" columnClassName="col-sm-3" dataField='familyName' filter={{type:'TextFilter', delay:0}} inverse>Surname</TableHeaderColumn>
            <TableHeaderColumn className="col-sm-3 text-center align-middle" columnClassName="col-sm-3" isKey dataField='dateOfBirth'>Date of Birth (yyyy/mm/dd)</TableHeaderColumn>
            <TableHeaderColumn className="col-sm-3 text-center" columnClassName="col-sm-3" dataField='nationality' filter={{type:'TextFilter'}}>Nationality</TableHeaderColumn>
          </BootstrapTable>
        </div>

          <Weather error={this.state.error} weatherFinder={this.findWeather} forecast={this.state.weather} place={this.state.place} timestamp={this.state.timestamp} active={this.state.active} />


      </div>
    );
  }

}

export default App;
