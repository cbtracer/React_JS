import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';


let baseUrl = 'http://ergast.com/api/f1/drivers.json';
let weatherUrl = 'http://api.openweathermap.org/data/2.5/forecast?q=';
let apiKey = '&appid=6d24f902bb2b3105900d54336a75e6dc';
ReactDOM.render(
  <App baseUrl={baseUrl} weatherUrl={weatherUrl} apiKey={apiKey} />,
  document.getElementById('root')
);
