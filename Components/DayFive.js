import React from 'react';

const DayFive = ({forecast, timestamp, place}) => {

  let segments = forecast.map((timeSeg) => {
    let unixCode = (new Date(timeSeg.dt*1000)).getDate();

    if (unixCode === timestamp + 4){
    // let a = new Date(unixCode*1000);
    // let year = a.getFullYear();
    // let month = a.getMonth() + 1;
    // let day = a.getDate();
    let temperature = Math.round(timeSeg.main.temp - 273.15);
    return (
      <div key={timeSeg.dt_txt} className="hor-pad small">
            <p className="row">{timeSeg.dt_txt.slice(10)} </p>
            <p className="row">{timeSeg.weather[0].description}</p>
            <p className="row">{temperature}<sup>o</sup>C</p>
      </div>
    )
  }else {
    return null
  }
  })

  return (

          <div className="card text-center black col-sm-6 offset-sm-3">
            <div className="card-header">
              {place.name} {place.country}
            </div>
            <div className="card-block">
              <h4 className="card-title"></h4>
              <div className="card-text d-flex">
                {segments}
              </div>
            </div>
          </div>

  )
}

export default DayFive;
