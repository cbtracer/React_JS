import React from 'react';
import DayOne from './DayOne.js';
import DayTwo from './DayTwo.js';
import DayThree from './DayThree.js';
import DayFour from './DayFour.js';
import DayFive from './DayFive.js';


const TimeWeather = ({forecast, place, timestamp}) => {

  return (
        <div>
          <DayOne forecast={forecast} timestamp={timestamp} place={place} />
          <DayTwo forecast={forecast} timestamp={timestamp} place={place} />
          <DayThree forecast={forecast} timestamp={timestamp} place={place} />
          <DayFour forecast={forecast} timestamp={timestamp} place={place} />
          <DayFive forecast={forecast} timestamp={timestamp} place={place} />
        </div>
  )
}


export default TimeWeather;
