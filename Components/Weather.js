import React from 'react';
import TimeWeather from './TimeWeather.js';

const Weather = ({error, weatherFinder, forecast, place, timestamp, active}) => {


  return(
    <div className="container-fluid weather white">
      <h2>The weather app</h2>
      <p>This uses an api from <a href="https://openweathermap.org/api"><u className="white">openweathermap.org</u></a>.</p>
      <p>By searching for a city, data is returned in JSON format and is then rendered to the page.</p>
      <hr />
      <div className="container white">
        <h2>What's happening with the weather?</h2>
        <form className="form-group">
          <label className="col-sm-4 text-center offset-sm-4 pad">Where would you like the weather for? <br /> To be unambiguous, please use the format city, uk</label>
          <input type="text" id="weather" name="city" placeholder="City" className="form-control col-sm-4 text-center offset-sm-4 pad" />
          <button className="btn btn-success btn-md col-sm-2 offset-sm-5" onClick={weatherFinder}>Search</button>
        </form>
        <div id="forecast">
          {error}
        </div>
        {active ?
        <div className="giveMeSpace" id="timely">
        <TimeWeather forecast={forecast} place={place} timestamp={timestamp} />
      </div> : null}
      </div>
    </div>
  )
}

export default Weather;
