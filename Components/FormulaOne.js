import React from 'react';

const FormulaOne = () => {
  return (
    <div className="container">
      <h3>Formula 1 drivers</h3>
      <p>This section uses an api found at: <a href="http://ergast.com/mrd/" target="_blank"><u>http://ergast.com/mrd/</u></a> which returns information about all F1 drivers past and present.</p>
      <p>You can search for a specific driver by utilising the search boxes below and you can also read their wikipedia page by clicking on the row of the driver you are interested in.</p>
      <p>This table and pagination system was built using <a href="http://allenfang.github.io/react-bootstrap-table/index.html" target="_blank"><u>react-bootstrap-table</u></a>.</p>
  </div>
  )
}


export default FormulaOne;
