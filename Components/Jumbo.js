import React from 'react';
import { Jumbotron, Button } from 'react-bootstrap/lib';
import logo from '../logo.svg';

const Jumbo = () => {
  return(
    <Jumbotron id="head" className="white">
    <h1>Welcome to my React App <img src={logo} className="App-logo" alt="logo" /></h1>
    <p>This page is built entirely using ReactJS and is designed as a place for me to experiment with handling data from external sources.</p>
    <hr />
    <p>This is also styled using the Bootstrap 4 library.</p>
    <p><Button bsStyle="primary">Learn more</Button></p>
  </Jumbotron>
  )
}

export default Jumbo;
